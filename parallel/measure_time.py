from time import time

from numpy.random import rand

from solve_system_of_linear_algebraic_equations import \
    solve_system_of_linear_algebraic_equations

N = 256

coefficient_matrix = rand(N, N)
constant_terms = rand(N)

start = time()
solve_system_of_linear_algebraic_equations(coefficient_matrix, constant_terms)
print(time() - start)
