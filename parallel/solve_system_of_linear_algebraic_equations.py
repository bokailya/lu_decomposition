"""LU Decomposition

inverse_permutation(permutation)
Return inverse permutation

lu_decomposition(matrix)
Construct the LU decomposition with partial pivoting of the input matrix.

restore(LU, row_permutation)
Restore matrix from LU decomposition
"""
from threading import Barrier, Event, Lock, Thread
from multiprocessing import cpu_count
from numpy import arange, argmax, zeros
from utility import to_column_vector, to_row_vector


def solve_system_of_linear_algebraic_equations(
        coefficient_matrix, constant_terms):
    """Solve square system of linear algebraic equations
    with nonsingular coefficient matrix using LU-decomposition

    Paramets:

    coefficient_matrix -- ndarray shape(size, size)
    constant_terms -- array of length size

    Returns:
    solution -- ndarray shape(size) with variable values
    """
    global_max = [0]
    global_max_index = [0]
    lock = Lock()
    matrix = coefficient_matrix.copy()
    number_of_threads = cpu_count()
    barrier = Barrier(number_of_threads)
    size = len(constant_terms)
    found = [Event() for i in range(size)]
    row_permutation = arange(size, dtype=int)
    solution = zeros(size)
    y = zeros(size)

    def thread_target(thread_number):
        """
        Job for single thread

        Paramets:

        thread_number -- id from 0 to number of threads - 1
        """
        def lu_decomposition():
            """Construct the LU decomposition
            with partial pivoting of the input matrix
            with uint diagonal elements of L
            """

            for i in range(size - 1):
                # Find pivot
                if i + thread_number < size:
                    local_max_index = i + thread_number + argmax(abs(matrix[
                        row_permutation[
                            i + thread_number::number_of_threads], i])) *\
                                    number_of_threads
                    local_max = abs(matrix[row_permutation[local_max_index], i])
                    with lock:
                        if global_max[0] < local_max:
                            global_max[0] = local_max
                            global_max_index[0] = local_max_index
                barrier.wait()
                if thread_number == 0:
                    if i != global_max_index[0]:
                        row_permutation[[
                            i, global_max_index[0]]] = row_permutation[[
                                global_max_index[0], i]]
                barrier.wait()
                for j in range(i + 1 + thread_number, size, number_of_threads):
                    gamma = matrix[row_permutation[j], i] / matrix[
                            row_permutation[i], i]
                    matrix[row_permutation[j], i + 1:] -= gamma * matrix[
                            row_permutation[i], i + 1:]
                    matrix[row_permutation[j], i] = gamma
                if thread_number == 0:
                    global_max[0] = 0
                barrier.wait()

        lu_decomposition()
        # Solve LY=B
        for i in range(thread_number, size, number_of_threads):
            y[i] = constant_terms[row_permutation[i]]
            for j in range(i):
                found[j].wait()
                y[i] -= matrix[row_permutation[i], j] * y[j]
            found[i].set()

        barrier.wait()
        for i in range(thread_number, size, number_of_threads):
            found[i].clear()
        barrier.wait()

        # Solve UX=Y
        for i in range(size - 1 - thread_number, -1, -number_of_threads):
            solution[i] = y[i]
            for j in range(i + 1, size):
                found[j].wait()
                solution[i] -= matrix[row_permutation[i], j] * solution[j]
            solution[i] /= matrix[row_permutation[i], i]
            found[i].set()

    # Create threads
    workers = [Thread(target=thread_target, args=(
        i,)) for i in range(number_of_threads - 1)]

    # Run threads
    for worker in workers:
        worker.start()
    thread_target(number_of_threads - 1)

    # Join threads
    for worker in workers:
        worker.join()
    return solution
