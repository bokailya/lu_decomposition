#!/usr/bin/env python
"""Test LU Decomposition"""
from unittest import TestCase, main
from numpy import fromfunction, ravel
from numpy.testing import assert_allclose
from solve_system_of_linear_algebraic_equations import \
    solve_system_of_linear_algebraic_equations
from utility import to_column_vector


class TestSolveSystemOfLinearAlgebraicEquations(TestCase):
    """Test function
    solve_system_of_linear_algebraic_equations_with_nonsingular_matrix
    """

    N = 6
    CONSTANT_TERMS = [2, 3, 5, 7, 11, 13]

    @staticmethod
    def test_not_require_pivoting():
        """Simple test case with size N"""
        coefficient_matrix = fromfunction(lambda i, j: 3 / (0.6*i*j + 1), (
            TestSolveSystemOfLinearAlgebraicEquations.N,
            TestSolveSystemOfLinearAlgebraicEquations.N))
        assert_allclose(ravel(coefficient_matrix @ to_column_vector(
            solve_system_of_linear_algebraic_equations(
                coefficient_matrix,
                TestSolveSystemOfLinearAlgebraicEquations.CONSTANT_TERMS))),
            TestSolveSystemOfLinearAlgebraicEquations.CONSTANT_TERMS)

    @staticmethod
    def test_require_pivoting():
        """Test case with size N that requires pivoting"""
        coefficient_matrix = fromfunction(lambda i, j: 3 / (0.6*i*j + 1), (
            TestSolveSystemOfLinearAlgebraicEquations.N,
            TestSolveSystemOfLinearAlgebraicEquations.N))
        coefficient_matrix[1, 1] = 3
        assert_allclose(ravel(coefficient_matrix @ to_column_vector(
            solve_system_of_linear_algebraic_equations(
                coefficient_matrix,
                TestSolveSystemOfLinearAlgebraicEquations.CONSTANT_TERMS))),
            TestSolveSystemOfLinearAlgebraicEquations.CONSTANT_TERMS)


if __name__ == '__main__':
    main()
