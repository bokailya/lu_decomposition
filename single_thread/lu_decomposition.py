"""LU Decomposition

inverse_permutation(permutation)
Return inverse permutation

lu_decomposition(matrix)
Construct the LU decomposition with partial pivoting of the input matrix.

restore(LU, row_permutation)
Restore matrix from LU decomposition
"""
from numpy import arange, argmax, zeros
from utility import to_column_vector, to_row_vector


def lu_decomposition(source_matrix):
    """Construct the LU decomposition with partial pivoting of the input matrix
    with uint diagonal elements of L

    Paramets:

    source_matrix -- ndarray shape(size, size)


    Returns:

    lu -- ndarray shape(size, size)
    matrix containing LU decomposition with diagonal elements of U matrix,
    L have unit diagonal elements
    """
    size = len(source_matrix)

    lu_matrices = source_matrix.copy()
    row_permutation = arange(size, dtype=int)
    for i in range(size - 1):
        max_index = i + argmax(abs(lu_matrices[row_permutation[i:], i]))
        if i != max_index:
            row_permutation[[i, max_index]] = row_permutation[[max_index, i]]
        gamma = lu_matrices[row_permutation[i + 1:], i] / lu_matrices[
            row_permutation[i], i]
        lu_matrices[row_permutation[i + 1:], i + 1:] -= to_column_vector(
                gamma) @ to_row_vector(lu_matrices[row_permutation[i], i + 1:])
        lu_matrices[row_permutation[i + 1:], i] = gamma
    return lu_matrices, row_permutation


def solve_system_of_linear_algebraic_equations(
        coefficient_matrix, constant_terms):
    """Solve square system of linear algebraic equations
    with nonsingular coefficient matrix using LU-decomposition

    Paramets:

    coefficient_matrix -- ndarray shape(size, size)
    constant_terms -- array of length size

    Returns:
    solution -- ndarray shape(size) with variable values
    """
    lu_matrices, row_permutation = lu_decomposition(coefficient_matrix)
    size = len(constant_terms)

    # Solve LY=B
    y = zeros(size)
    for i in range(size):
        y[i] = constant_terms[row_permutation[i]] - lu_matrices[
            row_permutation[i], :i].dot(y[:i])

    # Solve UX=Y
    solution = zeros(size)
    for i in range(size - 1, -1, -1):
        solution[i] = (y[i] - lu_matrices[row_permutation[i], i:].dot(
            solution[i:])) / lu_matrices[row_permutation[i], i]
    return solution
