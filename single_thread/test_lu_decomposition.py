#!/usr/bin/env python
"""Test LU Decomposition"""
from unittest import TestCase, main
from numpy import array, concatenate, empty, fromfunction, ravel
from numpy.testing import assert_allclose
from lu_decomposition import (
        lu_decomposition, solve_system_of_linear_algebraic_equations)
from utility import to_column_vector


class TestLuDecomposition(TestCase):
    """Test function lu_decomposition"""

    N = 6

    @staticmethod
    def inverse_permutation(permutation):
        """Return inverse permutation

        Paramets:

        permutation -- ndarray with shape (size,) containing permutation


        Returns:

        ndarray with shape(size,) containing inverse permutation
        """
        result = empty(len(permutation), dtype=int)
        for i in range(permutation.size):
            result[permutation[i]] = i
        return result

    @staticmethod
    def restore(lu_matrices, row_permutation):
        """Restore matrix from LU decomposition

        Paramets:
        lu -- ndarray shape(size, size) containing lu decomposition

        row_permutation -- ndarray shape(size,) containing permutation of rows

        Returns:
        result -- ndarray shape(size, size)
        containing matrix before decomposition
        """
        size = len(row_permutation)
        return array([[concatenate((lu_matrices[row_permutation[i], :min(
            i, j + 1)], [1] if i <= j else [])).dot(lu_matrices[
                row_permutation[:min(i, j + 1) + (
                    1 if i <= j else 0)], j]) for j in range(
                        size)] for i in range(size)])[
                                TestLuDecomposition.inverse_permutation(
                                    row_permutation)]

    @staticmethod
    def test_not_require_pivoting():
        """Simple test case with size N"""
        source_matrix = fromfunction(lambda i, j: 3 / (0.6*i*j + 1), (
            TestLuDecomposition.N, TestLuDecomposition.N))
        assert_allclose(TestLuDecomposition.restore(*lu_decomposition(
            source_matrix)), source_matrix)

    @staticmethod
    def test_require_pivoting():
        """Test case with size N that requires pivoting"""
        source_matrix = fromfunction(lambda i, j: 3 / (0.6*i*j + 1), (
            TestLuDecomposition.N, TestLuDecomposition.N))
        source_matrix[1, 1] = 3
        assert_allclose(TestLuDecomposition.restore(*lu_decomposition(
            source_matrix)), source_matrix)


class TestSolveSystemOfLinearAlgebraicEquations(TestCase):
    """Test function
    solve_system_of_linear_algebraic_equations_with_nonsingular_matrix
    """

    N = 6
    CONSTANT_TERMS = [2, 3, 5, 7, 11, 13]

    @staticmethod
    def test_not_require_pivoting():
        """Simple test case with size N"""
        coefficient_matrix = fromfunction(lambda i, j: 3 / (0.6*i*j + 1), (
            TestLuDecomposition.N, TestLuDecomposition.N))
        assert_allclose(ravel(coefficient_matrix @ to_column_vector(
            solve_system_of_linear_algebraic_equations(
                coefficient_matrix,
                TestSolveSystemOfLinearAlgebraicEquations.CONSTANT_TERMS))),
            TestSolveSystemOfLinearAlgebraicEquations.CONSTANT_TERMS)

    @staticmethod
    def test_require_pivoting():
        """Test case with size N that requires pivoting"""
        coefficient_matrix = fromfunction(lambda i, j: 3 / (0.6*i*j + 1), (
            TestLuDecomposition.N, TestLuDecomposition.N))
        coefficient_matrix[1, 1] = 3
        assert_allclose(ravel(coefficient_matrix @ to_column_vector(
            solve_system_of_linear_algebraic_equations(
                coefficient_matrix,
                TestSolveSystemOfLinearAlgebraicEquations.CONSTANT_TERMS))),
            TestSolveSystemOfLinearAlgebraicEquations.CONSTANT_TERMS)


if __name__ == '__main__':
    main()
