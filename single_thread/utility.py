"""Utility function"""
from numpy import newaxis as _newaxis


def to_column_vector(numpy_array):
    """Make column vector from 1 dimensional vector"""
    return numpy_array[:, _newaxis]


def to_row_vector(numpy_array):
    """Make row vector from 1 dimensional vector"""
    return numpy_array[_newaxis]
